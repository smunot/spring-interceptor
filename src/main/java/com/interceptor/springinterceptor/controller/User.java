package com.interceptor.springinterceptor.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class User {

    @GetMapping("/users")
    public List<String> getUsers(){
        List<String> userList = new ArrayList<>();
        userList.add("A");
        userList.add("B");
        userList.add("C");
        return userList;
    }
}
