package com.interceptor.springinterceptor.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Product {

    @GetMapping("/products")
    public List<String> getProduct(@RequestParam("name") String name){
        List<String> productList = new ArrayList<>();
        productList.add("Shailesh");
        productList.add("Vaibhav");
        productList.add("Gaurav");
        return productList;
    }
}
