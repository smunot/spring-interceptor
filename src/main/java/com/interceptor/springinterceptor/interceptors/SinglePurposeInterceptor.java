package com.interceptor.springinterceptor.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SinglePurposeInterceptor implements HandlerInterceptor {
    private final Logger LOGGER = LoggerFactory.getLogger(SinglePurposeInterceptor.class);
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LOGGER.info("in SinglePurposeInterceptor in preHandle Method");
        String name = request.getParameter("name");
        System.out.println(name);
        if(name.startsWith("s")){
            response.getWriter().println("Invalid Name");
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        LOGGER.info("in SinglePurposeInterceptor in postHandle Method");

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        LOGGER.info("in SinglePurposeInterceptor in afterCompletion Method");
    }
}
